import {View, Text} from 'react-native';
import React from 'react';
import Home from './src/Home/Home';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Route from './src/Router/Router';
import {NavigationContainer} from '@react-navigation/native';

const App = () => {
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Route />
    </NavigationContainer>
  );
};

export default App;
