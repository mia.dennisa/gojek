import {View, Text} from 'react-native';
import React from 'react';

const Header = ({title}) => {
  return (
    <View
      style={{
        height: '30%',
        width: '100%',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#00b300',
        // borderBottomWidth: 1,
        // borderColor: 'green',
      }}>
      <Text
        style={{
          fontWeight: 'bold',
          fontSize: 20,
          color: '#ffffff',
          letterSpacing: 1,
        }}>
        {title}
      </Text>
    </View>
  );
};

export default Header;
