import {View, Text, BackHandler} from 'react-native';
import React, {useEffect} from 'react';
import Header from '../Component/Header';

const Gosend = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  });
};

export default Gosend;
