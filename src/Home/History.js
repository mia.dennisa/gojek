import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Header from '../Component/Header';
import Paylater from '../../Image/paylater.png';

const History = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  const [result, setResult] = useState([]);
  const getData = async () => {
    var raw = '';

    var requestOptions = {
      method: 'GET',
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(response => {
        const value = JSON.parse(response);
        setResult(Object.entries(value));
      })
      .catch(error => console.log('error', error));
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  useEffect(() => {
    getData();
  }, []);

  return (
    <View style={styles.container}>
      <View style={{alignItems: 'flex-start'}}>
        <Text
          style={{
            // textAlign: 'left',
            color: 'black',
            fontWeight: 'bold',
            fontSize: 24,
            marginBottom: 10,
            marginTop: 10,
            marginLeft: 20,
          }}>
          Semua Transaksi
        </Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.body}>
          {result.map((e, index) => {
            return (
              <View style={styles.list} key={index}>
                <View>
                  <Text style={styles.data}>ID : {e[0]}</Text>
                  <Text style={styles.data}>Tujuan : {e[1].target}</Text>
                  <Text style={styles.data}>Transaksi : {e[1].type}</Text>
                  <Text style={styles.data}>Biaya : {e[1].amount}</Text>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    backgroundColor: '#ccccc',
    borderBottomWidth: 0.5,
    borderColor: 'black',
    width: 415,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  data: {
    fontWeight: '600',
    color: 'black',
    fontFamily: 'Montserrat-Regular',
  },
});

export default History;
