import {View, Text, BackHandler} from 'react-native';
import React, {useEffect} from 'react';

const Search = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  });
  return (
    <View>
      <Text>Search</Text>
    </View>
  );
};

export default Search;
