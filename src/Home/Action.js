import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const Getone = () => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var axios = require('axios');
    var data = JSON.stringify({
      amount: 1000,
      sender: 'NMalhNh5LPYMCq7uuUh',
      target: '-NMalhNh5LPYMCq7uuUh',
      type: 'transfer',
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };
  };
};

export const Transaksi = (am, send, tar, ty) => {
  return async dispatch => {
    var data = JSON.stringify({
      amount: am,
      sender: send,
      target: tar,
      type: ty,
    });

    var config = {
      method: 'post',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'content-type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        const data = JSON.stringify(response.data);
        console.log(data, 'berhasil');
        dispatch({
          type: 'GET_TRANSAKSI',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
    return true;
  };
};
