import {Image, View} from 'react-native';
import React, {useEffect} from 'react';
import Logo from '../../Image/logo.png';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Home');
    }, 2000);
  });
  return (
    <View
      style={{
        backgroundColor: '#FFFFFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={Logo}
        style={{height: 160, width: 200, resizeMode: 'center'}}
      />
    </View>
  );
};

export default SplashScreen;
