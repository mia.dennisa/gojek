import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../Home/Home';
import Bayar from '../Home/Bayar';
import Goride from '../Home/Goride';
import Gofood from '../Home/Gofood';
import GoCar from '../Home/Gocar';
import Bluebird from '../Home/Bluebird';
import Gosend from '../Home/Gosend';
import GoPoint from '../Home/Gopoint';
import Lainnya from '../Home/Lainnya';
import GoPulsa from '../Home/Gopulsa';
import History from '../Home/History';
import SplashScreen from '../Splashscreen/index';

const Stack = createNativeStackNavigator();

function Route() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="History"
        component={History}
        options={{title: 'Pesanan'}}
      />
      <Stack.Screen name="Bayar" component={Bayar} />
      <Stack.Screen name="Goride" component={Goride} />
      <Stack.Screen name="Gocar" component={GoCar} />
      <Stack.Screen name="Gofood" component={Gofood} />
      <Stack.Screen name="Bluebird" component={Bluebird} />
      <Stack.Screen name="Gosend" component={Gosend} />
      <Stack.Screen name="Gopulsa" component={GoPulsa} />
      <Stack.Screen name="Gopoint" component={GoPoint} />
      <Stack.Screen
        name="Lainnya"
        component={Lainnya}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}

export default Route;
